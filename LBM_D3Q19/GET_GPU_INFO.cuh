#pragma once

#include "datatype.h"
#include <string>
#include <cuda_runtime.h>
#include <vector>

class _GPU_INFO : public cudaDeviceProp {

private:	
	LBM_DATATYPE::si32 device_count;
	std::vector<cudaDeviceProp> DeviceProp;
	
public:
	_GPU_INFO();
	void load_device_count();
	void load_device_prop();
	const std::string get_device_name(LBM_DATATYPE::si32 number) const ;
	const std::vector<cudaDeviceProp> get_gpu_info() const;
	const LBM_DATATYPE::si32 get_async_engine(LBM_DATATYPE::si32 number) const;
	const LBM_DATATYPE::si32 get_device_count() const;

};

extern "C" {

	__declspec(dllexport) const std::vector<cudaDeviceProp> GET_GPU_INFO(_GPU_INFO*);
	__declspec(dllexport) const LBM_DATATYPE::si32 get_device_count(_GPU_INFO*);
	__declspec(dllexport) const std::string get_device_name(_GPU_INFO*, LBM_DATATYPE::si32);
	__declspec(dllexport) const LBM_DATATYPE::si32 get_async_engine(_GPU_INFO*, LBM_DATATYPE::si32 number);

}
