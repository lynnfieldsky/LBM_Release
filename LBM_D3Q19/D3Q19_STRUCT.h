#pragma once

using namespace LBM_DATATYPE;
using namespace std;

namespace LBM_D3Q19 {


	struct _BOUNDING_BOX {

		bool DIVISIBLE = true;
		bool DIVIDED = false;

		fl64 start_point[3];
		fl64 box_size[3];
		vector<si32> face_connected;
		vector<_BOUNDING_BOX> child_box;



	};


	class _TREE_PARTITION {

	public:
		si32 level;
		bool isdivided = false;
		
		Point_3 coordinate;

		const _TREE_PARTITION& parent;
		vector<_TREE_PARTITION> children;
		





		si32 bfsSetIndex(si32& indCur, const si08 l)
		{
			if (l > level)
			{
				if (!isLeaf())
				{
					for (si32 i = 0; i < 4; ++i)
					{
						children[i].bfsSetIndex(indCur, l);
					}
				}
			}
			else if (l == level)
			{
				value.cellInd = indCur;
				indCur++;
			}

			return indCur;
		}



	private:


		bool isLeaf() {

			return children.empty();

		}

	



	};


	enum Processors{ SINGLE_CORE, MULTI_CORE, GPU };


	struct _INPUT {
		_INPUT() {};
		_INPUT(bool a, bool b, ui08 c, bool d, ui32 e, ui64 f, ui64 g, fl64 h, FLUIDS i, vector<ui32> j) { 
		
			initialized = a;
			multi_threaded = b;
			target_Processor = c;
			turbulence = d;
			OPENMP_THREAD_COUNT = e;
			TARGET_ITER = f;
			auto_output_interval = g;
			initial_dx = h;
			PROJECT_MATERIAL = i;
			USING_GPU_list = j;	
		
		};

		bool initialized = false;

		bool multi_threaded = false;
		ui08 target_Processor = Processors::SINGLE_CORE;
		bool turbulence = false;

		ui32 OPENMP_THREAD_COUNT = 1;
		ui64 TARGET_ITER = 0;
		ui64 auto_output_interval = 10;

		fl64 initial_dx = 0;

		FLUIDS PROJECT_MATERIAL;
		vector<ui32> USING_GPU_list;

	};

	

	struct _INTERNAL {


		ui32 GPU_count = 0;
		ui32 GRID_BLOCK_TOTAL_NUM = 0;
		ui32 GRID_ST_NUM = 0;
		ui32 GRID_RT_NUM = 0;
		ui64 global_iteration = 0;


		vector<ui32> AVAILABLE_GPU_list;







	};


	struct _DEVICE_const {

		double e_i[57];
		double w[3];
		double cs_square;

		_DEVICE_const() {

			e_i[0 * 3 + 0] = 0.;		e_i[0 * 3 + 1] = 0.;		e_i[0 * 3 + 2] = 0.;
			e_i[1 * 3 + 0] = 1.;		e_i[1 * 3 + 1] = 0.;		e_i[1 * 3 + 2] = 0.;
			e_i[2 * 3 + 0] = -1.;		e_i[2 * 3 + 1] = 0.;		e_i[2 * 3 + 2] = 0.;
			e_i[3 * 3 + 0] = 0.;		e_i[3 * 3 + 1] = 1.;		e_i[3 * 3 + 2] = 0.;
			e_i[4 * 3 + 0] = 0.;		e_i[4 * 3 + 1] = -1.;		e_i[4 * 3 + 2] = 0.;
			e_i[5 * 3 + 0] = 0.;		e_i[5 * 3 + 1] = 0.;		e_i[5 * 3 + 2] = 1.;
			e_i[6 * 3 + 0] = 0.;		e_i[6 * 3 + 1] = 0.;		e_i[6 * 3 + 2] = -1.;

			e_i[7 * 3 + 0] = 1.;		e_i[7 * 3 + 1] = 1.;		e_i[7 * 3 + 2] = 0.;
			e_i[8 * 3 + 0] = -1.;		e_i[8 * 3 + 1] = 1.;		e_i[8 * 3 + 2] = 0.;
			e_i[9 * 3 + 0] = 1.;		e_i[9 * 3 + 1] = -1.;		e_i[9 * 3 + 2] = 0.;
			e_i[10 * 3 + 0] = -1.;		e_i[10 * 3 + 1] = -1.;		e_i[10 * 3 + 2] = 0.;

			e_i[11 * 3 + 0] = 1.;		e_i[11 * 3 + 1] = 0.;		e_i[11 * 3 + 2] = 1.;
			e_i[12 * 3 + 0] = -1.;		e_i[12 * 3 + 1] = 0.;		e_i[12 * 3 + 2] = 1.;
			e_i[13 * 3 + 0] = 1.;		e_i[13 * 3 + 1] = 0.;		e_i[13 * 3 + 2] = -1.;
			e_i[14 * 3 + 0] = -1.;		e_i[14 * 3 + 1] = 0.;		e_i[14 * 3 + 2] = -1.;

			e_i[15 * 3 + 0] = 0.;		e_i[15 * 3 + 1] = 1.;		e_i[15 * 3 + 2] = 1.;
			e_i[16 * 3 + 0] = 0.;		e_i[16 * 3 + 1] = -1.;		e_i[16 * 3 + 2] = 1.;
			e_i[17 * 3 + 0] = 0.;		e_i[17 * 3 + 1] = 1.;		e_i[17 * 3 + 2] = -1.;
			e_i[18 * 3 + 0] = 0.;		e_i[18 * 3 + 1] = -1.;		e_i[18 * 3 + 2] = -1.;

			w[0] = 1. / 3.;
			w[1] = 1. / 18.;
			w[2] = 1. / 36.;

			cs_square = 1. / 3.;




		}


	};




	struct _ST_GRID_DEVICE {






	};


	struct _RT_GRID_DEVICE {






	};

	struct __declspec(align(CACHE_SIZE)) _ST_GRID_HOST {

		bool USE_BOUNDING_BOX = false;
		ui32 subdivision_count = 0;

		fl64 DX = 0;
		fl64 DT = 0;




		FLUIDS LBM_MATERIAL;
		vector<_ST_GRID_DEVICE> GRID_DEVICES;
		vector<_BOUNDING_BOX> BOUNDING_BOXES;


		//TODO 필요한 공간에 tree








	};

	struct __declspec(align(CACHE_SIZE)) _RT_GRID_HOST {

		bool USE_BOUNDING_BOX = false;
		ui32 subdivision_count = 0;

		vector<_RT_GRID_DEVICE> GRID_DEVICES;
		vector<_BOUNDING_BOX> BOUNDING_BOXES;



	};




	

}