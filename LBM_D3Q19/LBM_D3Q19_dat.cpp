﻿// pch.cpp: 미리 컴파일된 헤더에 해당하는 소스 파일

#include "LBM_D3Q19_dat.h"

// 미리 컴파일된 헤더를 사용하는 경우 컴파일이 성공하려면 이 소스 파일이 필요합니다.

using namespace LBM_DATATYPE;
using namespace std;

LBM_D3Q19::_PROJECT_PARAMS::_PROJECT_PARAMS() {
	



}
LBM_D3Q19::_INPUT LBM_D3Q19::_PROJECT_PARAMS::get_INPUT() const { return INPUT; }
ui32 LBM_D3Q19::_PROJECT_PARAMS::set_INPUT(_INPUT a) { try { INPUT = a; } catch (const std::exception& e) { return 1; } return 0; }
inline bool LBM_D3Q19::_PROJECT_PARAMS::is_multi_threaded() const { return INPUT.multi_threaded; }


