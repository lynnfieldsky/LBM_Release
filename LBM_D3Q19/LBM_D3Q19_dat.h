﻿// pch.h: 미리 컴파일된 헤더 파일입니다.
// 아래 나열된 파일은 한 번만 컴파일되었으며, 향후 빌드에 대한 빌드 성능을 향상합니다.
// 코드 컴파일 및 여러 코드 검색 기능을 포함하여 IntelliSense 성능에도 영향을 미칩니다.
// 그러나 여기에 나열된 파일은 빌드 간 업데이트되는 경우 모두 다시 컴파일됩니다.
// 여기에 자주 업데이트할 파일을 추가하지 마세요. 그러면 성능이 저하됩니다.
#pragma once

#ifndef _D3Q19_DAT
#define _D3Q19_DAT
#define CACHE_SIZE sizeof(LBM_DATATYPE::fl64) * 8

#include "framework.h"
#include "datatype.h"
#include "GET_GPU_INFO.cuh"
#include <iostream>
#include <vector>
#include "D3Q19_STRUCT.h"
#include <string>


using namespace LBM_DATATYPE;
using namespace std;

namespace LBM_D3Q19 {



	class _PROJECT_PARAMS {

	public:
		_PROJECT_PARAMS();
		const si32 get_DEVICE_COUNT() const;
		_INPUT get_INPUT() const;
		ui32 set_INPUT(_INPUT);
		si32 set_LBM_PARAMS();
		inline bool is_multi_threaded() const;

		_INPUT INPUT;
		_INTERNAL INTERNAL;
		_DEVICE_const DEVICE_CONST;

		_ST_GRID_HOST ST_GRID_HOST;
		_RT_GRID_HOST RT_GRID_HOST;
		_GPU_INFO GPU_INFO;

	private:
		
		

	};
		
	si32 get_DEVICE_COUNT(_PROJECT_PARAMS*);
	

	


}




#endif
