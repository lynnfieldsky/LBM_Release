#include "GET_GPU_INFO.cuh"

_GPU_INFO::_GPU_INFO() {
	load_device_count();
	load_device_prop();
}
void _GPU_INFO::load_device_count() {
	cudaError_t cu_err;
	cu_err = cudaGetDeviceCount(&device_count);
	if (cu_err != cudaSuccess) { device_count = -1; }
}
void _GPU_INFO::load_device_prop() {
	for (LBM_DATATYPE::si32 i = 0; i < device_count; i++) {
		cudaDeviceProp temp;
		cudaGetDeviceProperties(&temp, i);
		DeviceProp.emplace_back(temp);
	}
}
const std::string _GPU_INFO::get_device_name(LBM_DATATYPE::si32 number) const { return DeviceProp[number].name; }
const std::vector<cudaDeviceProp> _GPU_INFO::get_gpu_info() const { return DeviceProp; }
const LBM_DATATYPE::si32 _GPU_INFO::get_async_engine(LBM_DATATYPE::si32 number) const { return DeviceProp[number].asyncEngineCount; }
const LBM_DATATYPE::si32 _GPU_INFO::get_device_count() const { return device_count; }

const LBM_DATATYPE::si32 get_device_count(_GPU_INFO *device_prop) { return device_prop->get_device_count(); }
const std::vector<cudaDeviceProp> GET__GPU_INFO(_GPU_INFO *device_prop) { return device_prop->get_gpu_info(); }
const std::string get_device_name(_GPU_INFO *device_prop, LBM_DATATYPE::si32 number) { return device_prop->get_device_name(number); }
const LBM_DATATYPE::si32 get_async_engine(_GPU_INFO* device_prop, LBM_DATATYPE::si32 number) { return device_prop->get_async_engine(number); }
