﻿#pragma once

/*
 Copyright (c) 2018, Sebastian Reiter (s.b.reiter@gmail.com)
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include "framework.h"
#include <vector>
#include "datatype.h"
#include <QtCore/QString>
#include <utility>

#include <exception>
#include <iostream>
#include <thread>
#include <omp.h>
#include <random>
#include <limits.h>
#include <filesystem>

#include "stl_reader.h"


using namespace LBM_DATATYPE;

namespace OBJECT_PARSER {     

    class STL_Object {

      public:
        si32 eigenvalue;
        fl64 unit;
        Point_3 min_coords;
        Point_3 max_coords;    

        Point_3 tree_init_size;
        ui32 tree_division_level;
        ui32 max_tree_grid_size;

        std::vector<Mesh_tri> Tris;
        std::vector<ui32> Tris_in_z;

        ui08 write_tmp();
        ui08 read_tmp();

    };

	class _STL_PARSER : stl_reader::StlMesh<fl64, ui32> {

      
    public:

        _STL_PARSER(const QString, const fl64); 
        STL_Object STL_Mesh_out();

    private:

        ui32 CountCPU = 0;
        std::string filename;     
        
        std::vector<Mesh_tri> Tris;
        Point_3 min_coords;
        Point_3 max_coords;
        fl64 unit;
        inline std::string str_convert(const QString);
        void Parse();
        void unit_conversion();

	};




    extern "C" {

        __declspec(dllexport) STL_Object Load_STL(const QString, const fl64);
        __declspec(dllexport) ui08 write_STL_obj(OBJECT_PARSER::STL_Object);
       


      //__declspec(dllexport) _STL_PARSER::ReadStlFile(fl64, fl64, ui32, ui32);

    }

    

}

