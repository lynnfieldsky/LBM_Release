﻿#include "Object_parser.h"

ui08 OBJECT_PARSER::STL_Object::write_tmp() {

	bool err = false;

	if (std::filesystem::exists("./tmp") == false) {
		err = std::filesystem::create_directory("./tmp");
	}

	QString tmp_path = QString::number(eigenvalue) + ".tmp";
	if (std::filesystem::exists(tmp_path.toStdString()) == true) {

	}
	else {

		//binary file
		std::string header_info = std::to_string(eigenvalue) + "-output";
		char head[80];
		strncpy_s(head, header_info.c_str(), sizeof(head) - 1);
		char attribute[2] = "0";
		unsigned long nTriLong = Tris.size();

		std::ofstream myfile;

		myfile.open(std::string("tmp/") + (tmp_path.toStdString()).c_str(), std::ios::out | std::ios::binary);
		myfile.write(head, sizeof(head));
		myfile.write((char*)&nTriLong, sizeof(si32));

		std::vector<fl32[3]> normal_fl32(nTriLong);
		std::vector<fl32[9]> pt_fl32(nTriLong);

		for (si32 i = 0; i < nTriLong; i++) {

			normal_fl32[i][0] = Tris[i].normal[0];
			normal_fl32[i][1] = Tris[i].normal[1];
			normal_fl32[i][2] = Tris[i].normal[2];

			pt_fl32[i][0] = Tris[i].tri_points[0][0];
			pt_fl32[i][1] = Tris[i].tri_points[0][1];
			pt_fl32[i][2] = Tris[i].tri_points[0][2];
			pt_fl32[i][3] = Tris[i].tri_points[1][0];
			pt_fl32[i][4] = Tris[i].tri_points[1][1];
			pt_fl32[i][5] = Tris[i].tri_points[1][2];
			pt_fl32[i][6] = Tris[i].tri_points[2][0];
			pt_fl32[i][7] = Tris[i].tri_points[2][1];
			pt_fl32[i][8] = Tris[i].tri_points[2][2];

		}
		for (si32 i = 0; i < nTriLong; i++) {

			myfile.write(reinterpret_cast<char*>(&normal_fl32[i]), sizeof(fl32) * 3);
			myfile.write(reinterpret_cast<char*>(&pt_fl32[i]), sizeof(fl32) * 9);
			myfile.write(attribute, sizeof(attribute));

		}		

		myfile.close();
	}


	return err;


}

OBJECT_PARSER::_STL_PARSER::_STL_PARSER(const QString _filename, const fl64 _unit) {

	CountCPU = si32(ceil(std::thread::hardware_concurrency() * 0.8));
	omp_set_num_threads(CountCPU);	
	filename = str_convert(_filename);
	unit = _unit;
	Parse();	
	//unit_conversion();


};



void OBJECT_PARSER::_STL_PARSER::unit_conversion() {

	#pragma omp parallel for
	for (si32 i = 0; i < Tris.size(); i++) {
		Tris[i] *= unit;
	}

	min_coords *= unit;
	max_coords *= unit;

}

void OBJECT_PARSER::_STL_PARSER::Parse() {

	try {

		stl_reader::StlMesh <fl64, ui32> mesh(filename);
		Tris.resize(mesh.num_tris());

		std::vector<fl64> x_coords(mesh.num_tris() * 3);
		std::vector<fl64> y_coords(mesh.num_tris() * 3);
		std::vector<fl64> z_coords(mesh.num_tris() * 3);


		for (size_t itri = 0; itri < mesh.num_tris(); ++itri) {		

			for (size_t icorner = 0; icorner < 3; ++icorner) {

				const fl64* c = mesh.tri_corner_coords(itri, icorner);
				Point_3 corner_point;
				corner_point[0] = c[0];
				corner_point[1] = c[1];
				corner_point[2] = c[2];

				x_coords[3 * itri + icorner] = c[0];
				y_coords[3 * itri + icorner] = c[1];
				z_coords[3 * itri + icorner] = c[2];
				Tris[itri].tri_points[icorner] = corner_point;

			}
			Point_3 normal;
			const fl64* n = mesh.tri_normal(itri);
			normal[0] = n[0];
			normal[1] = n[1];
			normal[2] = n[2];
			
			Tris[itri].normal = normal;
			Tris[itri].idx = itri;

		}

		std::pair<std::vector<fl64>::iterator, std::vector<fl64>::iterator> min_max;
		min_max = std::minmax_element(x_coords.begin(), x_coords.end());
		min_coords[0] = *min_max.first; max_coords[0] = *min_max.second;

		min_max = std::minmax_element(y_coords.begin(), y_coords.end());
		min_coords[1] = *min_max.first; max_coords[1] = *min_max.second;

		min_max = std::minmax_element(z_coords.begin(), z_coords.end());
		min_coords[2] = *min_max.first; max_coords[2] = *min_max.second;

		//std::cout << min_coords[0] << " " << max_coords[0] << std::endl;

	}

	catch (std::exception& e) { std::cout << e.what() << std::endl; }

}

OBJECT_PARSER::STL_Object OBJECT_PARSER::_STL_PARSER::STL_Mesh_out() 
{	
	OBJECT_PARSER::STL_Object output;
	output.Tris.swap(Tris);
	output.min_coords = min_coords;
	output.max_coords = max_coords;
	output.unit = unit;

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<si32> dis(INT_MIN, INT_MAX);
	output.eigenvalue = dis(gen);
	
	return output;
}
inline std::string OBJECT_PARSER::_STL_PARSER::str_convert(const QString _filename) { return _filename.toStdString(); }




OBJECT_PARSER::STL_Object OBJECT_PARSER::Load_STL(const QString filename, const fl64 unit) {

	OBJECT_PARSER::_STL_PARSER STL_Object(filename, unit);

	return STL_Object.STL_Mesh_out();

}

ui08 OBJECT_PARSER::write_STL_obj(OBJECT_PARSER::STL_Object obj) {

	return obj.write_tmp();

}
