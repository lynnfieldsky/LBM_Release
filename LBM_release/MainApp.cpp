#include "include/MainWindow.h"

extern _MAINAPP::MainApp* _MAIN_WIDGET;

using namespace _MAINAPP;

MainApp::MainApp() {

	//this->setStyleSheet("QWidget {background-color : QLinearGradient(y1:0, y2:1, stop: 0 #7FDAFF, stop: 0.4 #3C96FF, stop: 0.2 #2B95FF, stop:1 #2B95FF) };");
	
}

MainApp::~MainApp() {
		
	if (LBM_thread != NULL) {

		if (LBM_thread->isRunning() == true) {

			try { LBM_thread->quit(); }
			catch (std::exception& e) { std::cout << e.what() << std::endl; }

		}

		delete[] LBM_thread;
		LBM_thread = NULL;

	}

}

void MainApp::start() {

	this->setObjectName(QString::fromUtf8("MainWindow"));
	this->resize(1000, 1000);

	MainWidget = new QWidget(this);
	CamWidget = new QWidget(this);
	MainLayout = new QGridLayout();
	MenuLayout = new QGridLayout();
	MainWidgetLayout = new QGridLayout();

	Cam_init();

	CamWidget = QWidget::createWindowContainer(Cam_Object->view);
	//CamWidget->setGeometry(QRect(50, 150, 800, 800));
	CamWidget->setMinimumSize(QSize(800, 800));
	CamWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);


	Load_STL_button = new QPushButton(MainWidget);
	//Load_STL_button->setGeometry(QRect(50, 50, 100, 50));
	Load_STL_button->setMinimumSize(QSize(100, 30));
	Load_STL_button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	Load_STL_button->setObjectName("Load_STL");
	connect(Load_STL_button, SIGNAL(released()), this, SLOT(Load_Main_Mesh_Widget()));
	
	Grid_init_button = new QPushButton(MainWidget);
	//Grid_init_button->setGeometry(QRect(200, 50, 100, 50));
	Grid_init_button->setMinimumSize(QSize(100, 30));
	Grid_init_button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	Grid_init_button->setObjectName("Grid_init");
	connect(Grid_init_button, SIGNAL(released()), this, SLOT(Grid_init()));

	Set_LBM_param_button = new QPushButton(MainWidget);
	Set_LBM_param_button->setMinimumSize(QSize(200, 30));
	Set_LBM_param_button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	Set_LBM_param_button->setObjectName("Param_init");
	connect(Set_LBM_param_button, SIGNAL(released()), this, SLOT(Set_LBM_param()));




	MenuLayout->addWidget(Load_STL_button, 0, 0);
	MenuLayout->addWidget(Grid_init_button, 0, 1);	
	MenuLayout->addWidget(Set_LBM_param_button, 0, 2);
	MenuLayout->setSizeConstraint(MenuLayout->SetFixedSize);
	MenuLayout->setRowMinimumHeight(0, 100);
	MenuLayout->setColumnMinimumWidth(0, 100);
	MenuLayout->setAlignment(Qt::AlignHCenter);
	MenuLayout->setAlignment(Qt::AlignLeft);
	MenuLayout->setSpacing(0);
	MenuLayout->setContentsMargins(0, 0, 0, 0);

	MainWidgetLayout->addWidget(CamWidget, 0, 0);
	MainWidgetLayout->setSpacing(0);
	MainWidgetLayout->setContentsMargins(0, 0, 0, 0);

	MainLayout->addLayout(MenuLayout, 0, 0);
	MainLayout->addLayout(MainWidgetLayout, 1, 0);
	MainLayout->setRowMinimumHeight(0, 40);
	MainLayout->setSpacing(0);
	MainLayout->setContentsMargins(0, 0, 0, 0);

	MainWidget->setLayout(MainLayout);

	retranslate();
	this->setCentralWidget(MainWidget);
	this->show();



}


void MainApp::Load_Main_Mesh_Widget() {

	if (Main_Mesh_Widget_allocated == false) {

		Main_Mesh_Object = new _Main_Mesh_Object();		

		connect(this, SIGNAL(Load_STL_Table_show()), Main_Mesh_Object, SLOT(Table_show()));
				
		emit Load_STL_Table_show();
		Main_Mesh_Widget_allocated = true;

	}

	else {

		emit Load_STL_Table_show();

	}
}


void MainApp::Grid_init() {

	if (Cam_Object->Meshes.size() == 0) {

		QMessageBox msgBox;
		//msgBox.setAttribute(Qt::WA_DeleteOnClose, true);
		//msgBox.setBaseSize(QSize(600, 120));		
		msgBox.setText("No mesh is selected");
		msgBox.setStandardButtons(QMessageBox::Cancel);
		msgBox.setDefaultButton(QMessageBox::Cancel);
		si32 result = msgBox.exec();

	}

	else {

		if (LBM_thread == NULL || LBM_thread->isRunning() == false) {

			QMessageBox msgBox;
			msgBox.setText("Please set Simulation parameters first");
			msgBox.setStandardButtons(QMessageBox::Cancel);
			msgBox.setDefaultButton(QMessageBox::Cancel);
			si32 result = msgBox.exec();

		}

		else {
			std::cout << this->thread() << " main" << std::endl;

			emit LBM_Backend_grid_setup_signal(STL_meshes);


			//LBM_backend->grid_setup(STL_meshes);

		}


	}

}

void MainApp::Set_LBM_param() {

	if (LBM_thread == NULL) {

		try {

			LBM_backend = new _LBM_Backend_thread();
			LBM_thread = new QThread();

			LBM_backend->moveToThread(LBM_thread);
			connect(LBM_backend, SIGNAL(finished()), LBM_thread, SLOT(quit()));
			connect(this, SIGNAL(LBM_Backend_grid_setup_signal(vector<OBJECT_PARSER::STL_Object>)),
				LBM_backend, SLOT(grid_setup(vector<OBJECT_PARSER::STL_Object>)));
			LBM_thread->start();


		}
		catch (const std::bad_alloc& e) {

			std::cout << e.what() << std::endl;

		}
	}

	if (LBM_backend->is_initialized() == false) {

		LBM_backend->initialize();

	}

	else {

		//TODO parameter set window

	}






}

void MainApp::add_STL(OBJECT_PARSER::STL_Object &loaded_STL) {

	STL_meshes.emplace_back(loaded_STL);

}





void MainApp::retranslate() {

	Load_STL_button->setText(QCoreApplication::translate("MainWindow", "Load STL", nullptr));
	Grid_init_button->setText(QCoreApplication::translate("MainWindow", "Grid initialize", nullptr));
	Set_LBM_param_button->setText(QCoreApplication::translate("MainWindow", "Set simulation parameters", nullptr));

}

void MainApp::Cam_init() {			

	if (Cam_Widget_allocated == false) {

		Cam_Object = new _Cam_Object();
				
		Cam_Widget_allocated = true;

	}


}


_MAINAPP::_Cam_Object::_Cam_Object() {
	
	
	parent_window = _MAIN_WIDGET;

	rootEntity = new Qt3DCore::QEntity();
	base_material = new Qt3DExtras::QPhongMaterial(rootEntity);
	view = new Qt3DExtras::Qt3DWindow();

	// Camera
	camera = view->camera();
	camera->lens()->setPerspectiveProjection(45.0f, 16.0f / 9.0f, 0.1f, 10000.0f);
	camera->setPosition(QVector3D(0, 0, 1000.0f));
	camera->setViewCenter(QVector3D(0, 0, 0));
	//camera->setUpVector(QVector3D(0.0, 0.0, 0.0));

	// For camera controls
	camController = new Qt3DExtras::QOrbitCameraController(rootEntity);
	camController->setLinearSpeed(-250.0f);
	camController->setLookSpeed(-150.0f);
	camController->setCamera(camera);
	
	Entities = new Qt3DCore::QEntity(rootEntity);
	Entities->addComponent(base_material);

	view->setRootEntity(rootEntity);
	view->renderSettings()->setRenderPolicy(Qt3DRender::QRenderSettings::OnDemand);

	view->show();

}


_MAINAPP::_Cam_Object::~_Cam_Object() {

	emit this->finished();

}

void _MAINAPP::_Cam_Object::add_Mesh(QString &path, Point_3& center) {

	Meshes.emplace_back(new Qt3DRender::QMesh(rootEntity));
	Meshes.back()->setSource(QUrl::fromLocalFile(path));
	Meshes.back()->setMeshName(path);
	Entities->addComponent(Meshes.back());
	camera->setViewCenter(QVector3D(center[0], center[1], center[2]));

}