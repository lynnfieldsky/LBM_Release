#include "include/Appstart.h"
#include <Qpropertyanimation.h>



//TODO////////
//Color DEFINE
//Each object location DEFINE
//return button

_APPSTART::Appstart::Appstart() {

	this->setObjectName(QString::fromUtf8("AppstartWindow"));
	this->setWindowFlag(Qt::FramelessWindowHint);

	this->resize(400, 800);
	this->setStyleSheet("QWidget {background-color : QLinearGradient(y1:0, y2:1, stop: 0 #7FDAFF, stop: 0.4 #3C96FF, stop: 0.2 #2B95FF, stop:1 #2B95FF) };");
	//this->setStyleSheet("QWidget {background-color : #2B95FF; };");

	QPainterPath path;
	path.addRoundedRect(this->rect(), 20, 20);
	QRegion mask = QRegion(path.toFillPolygon().toPolygon());	
	this->setMask(mask);
	QFont start_window_font("Cinzel Decorative");

	//QWidget text_widget;
	text_label = new QLabel(this);
	text_label->setGeometry(QRect(10, 150, 380, 50));
	text_label->setWindowFlag(Qt::FramelessWindowHint);
	text_label->setWindowOpacity(1);
	text_label->setObjectName("main_text");
	text_label->setStyleSheet("QLabel {color : #FFFFFF; font-size: 30pt; font-weight: bold; background-color : rgba(255, 255, 255, 0);};");
	text_label->setAlignment(Qt::AlignCenter);
	text_label->setFont(start_window_font);

	New_button = new QPushButton(this);
	New_button->setGeometry(QRect(100, 500, 200, 50));
	New_button->setObjectName(QString::fromUtf8("New"));
	New_button->setAttribute(Qt::WA_NoSystemBackground);
	New_button->setStyleSheet("QPushButton {background-color : #2B95FF; border-width: 3px; border-color: #FFFFFF; border-style: solid;	border-radius: 20; color : #FFFFFF; font-size: 12pt; font-weight: bold;};");
	connect(New_button, SIGNAL(released()), this, SLOT(New()));


	Load_button = new QPushButton(this);
	Load_button->setGeometry(QRect(100, 570, 200, 50));
	Load_button->setObjectName(QString::fromUtf8("Load"));
	Load_button->setStyleSheet("QPushButton {background-color : #2B95FF; border-width: 3px; border-color: #FFFFFF; border-style: solid; border-radius: 20;  color : #FFFFFF; font-size: 12pt; font-weight: bold;};");
	connect(Load_button, SIGNAL(released()), this, SLOT(Load()));


	Close_button = new QPushButton(this);
	Close_button->setGeometry(QRect(100, 640, 200, 50));
	Close_button->setObjectName(QString::fromUtf8("Close"));
	Close_button->setStyleSheet("QPushButton {background-color : #2B95FF;  border-width: 3px; border-color: #FFFFFF; border-style: solid; border-radius: 20;  color : #FFFFFF; font-size: 12pt; font-weight: bold;};");
	connect(Close_button, SIGNAL(released()), this, SLOT(close()));


	retranslateSTART();

}

void _APPSTART::Appstart::retranslateSTART() {

	New_button->setText(QCoreApplication::translate("AppstartWindow", "New Project", nullptr));
	Load_button->setText(QCoreApplication::translate("AppstartWindow", "Load Project", nullptr));
	Close_button->setText(QCoreApplication::translate("AppstartWindow", "Close", nullptr));
	text_label->setText(QCoreApplication::translate("AppstartWindow", "Aquamarine", nullptr));
}

void _APPSTART::Appstart::retranslateLBM() {

	D2Q9_button->setText(QCoreApplication::translate("AppstartWindow", "2D Project", nullptr));
	D3Q19_button->setText(QCoreApplication::translate("AppstartWindow", "3D Project", nullptr));
	
}

void _APPSTART::Appstart::mousePressEvent(QMouseEvent* event) {	offset = event->pos(); }
void _APPSTART::Appstart::mouseMoveEvent(QMouseEvent* event) { 
		
	if (event->buttons() & Qt::LeftButton)	{

		this->move(mapToParent(event->pos() - offset));

	}

}

_APPSTART::Appstart::~Appstart() {
	
	delete[] New_button;
	delete[] Load_button;
	delete[] Close_button;
	delete[] text_label;
	delete[] New_animation;
	delete[] D2Q9_button;
	delete[] D3Q19_button;

	New_button = NULL;
	Load_button = NULL;
	Close_button = NULL;
	text_label = NULL;
	New_animation = NULL;
	D2Q9_button = NULL;
	D3Q19_button = NULL;

}


void _APPSTART::Appstart::New() {

	Load_button->setVisible(false);
	Load_button->setCheckable(false);

	Close_button->setVisible(false);
	Close_button->setCheckable(false);
	


	New_animation = new QPropertyAnimation(New_button, "geometry");
	New_animation->setDuration(1000);
	New_animation->setStartValue(QRect(100, 500, 200, 50));
	New_animation->setEndValue(QRect(50, 300, 200, 50));
	New_animation->setEasingCurve(QEasingCurve::InOutCubic);	

	New_animation->start();
	connect(New_animation, SIGNAL(finished()), this, SLOT(show_LBM()));



}


void _APPSTART::Appstart::Load() {


}


void _APPSTART::Appstart::D2Q9() {


}

void _APPSTART::Appstart::D3Q19() {

	this->hide();

	_MAIN_WIDGET->start();
	
}

void _APPSTART::Appstart::show_LBM() {

	New_button->setEnabled(false);
	
	D2Q9_button = new QPushButton(this);
	D2Q9_button->setGeometry(QRect(100, 500, 200, 50));
	D2Q9_button->setObjectName(QString::fromUtf8("D2Q9"));
	D2Q9_button->setStyleSheet("QPushButton {background-color : #2B95FF; border-width: 3px; border-color: #FFFFFF; border-style: solid;	border-radius: 20; color : #FFFFFF; font-size: 12pt; font-weight: bold;};");
	connect(D2Q9_button, SIGNAL(released()), this, SLOT(D2Q9()));
	

	D3Q19_button = new QPushButton(this);
	D3Q19_button->setGeometry(QRect(100, 570, 200, 50));
	D3Q19_button->setObjectName(QString::fromUtf8("D3Q19"));
	D3Q19_button->setStyleSheet("QPushButton {background-color : #2B95FF; border-width: 3px; border-color: #FFFFFF; border-style: solid; border-radius: 20;  color : #FFFFFF; font-size: 12pt; font-weight: bold;};");
	connect(D3Q19_button, SIGNAL(released()), this, SLOT(D3Q19()));

	D2Q9_button->setVisible(true);
	D3Q19_button->setVisible(true);
	//Close_button = new QPushButton(this);
	//Close_button->setGeometry(QRect(100, 640, 200, 50));
	//Close_button->setObjectName(QString::fromUtf8("Close"));
	//Close_button->setStyleSheet("QPushButton {background-color : #2B95FF;  border-width: 3px; border-color: #FFFFFF; border-style: solid; border-radius: 20;  color : #FFFFFF; font-size: 12pt; font-weight: bold;};");
	//connect(Close_button, SIGNAL(released()), this, SLOT(close()));


	retranslateLBM();



}
