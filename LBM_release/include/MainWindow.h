#pragma once
#include "MainWindowIncludes.h"




namespace _MAINAPP {
	

	QStringList FileDialog(QWidget*, QString);
	class MainApp;
	class Load_STL_Window;


	class _LBM_Backend_thread : public QObject {

		Q_OBJECT

	private:
		
		MainApp* parent_window;
		LBM_D3Q19::_PROJECT_PARAMS PROJECT_PARAMS;
		

	signals:
		void finished();

	public:		

		_LBM_Backend_thread();
		~_LBM_Backend_thread();

		LBM_D3Q19::_INPUT get_INPUT() const;
		bool is_initialized() const;


		si32 initialize();
		


		si32 grid_gen_st(vector<OBJECT_PARSER::STL_Object>);
		si32 grid_gen_omp(vector<OBJECT_PARSER::STL_Object>);
		si32 grid_gen_gpu(vector<OBJECT_PARSER::STL_Object>);

	public slots:
		si32 grid_setup(vector<OBJECT_PARSER::STL_Object>);



	};




	class _Main_Mesh_Object : public QObject {

		Q_OBJECT

	public:


		_Main_Mesh_Object();
		_Main_Mesh_Object(MainApp*);
		~_Main_Mesh_Object();
		
		void TableInit();

	signals:
		void finished();
		void add_STL(OBJECT_PARSER::STL_Object&);
		void add_STL(QString&, Point_3&);


	public slots:

		
		si32 read_STL();
		void Table_show();

	private:
		bool initialized = false;
		MainApp* parent_window;

		QTableWidget* Table;		
		QStringList tableHeader;
		QPushButton* Table_add_new;


		std::vector<fl64> Mesh_units;
		std::vector<QPushButton*> Table_buttons;
		std::vector<QWidget*> Cell_widgets;
		std::vector<QGridLayout*> Layouts;


	};

	class _Cam_Object : public QObject {

		Q_OBJECT

	private:
		MainApp* parent_window;

	signals:
		void finished();
		
		
	public:
		_Cam_Object();
		~_Cam_Object();
		

		std::vector<Qt3DRender::QMesh*> Meshes;
		Qt3DCore::QEntity* rootEntity;
		Qt3DRender::QMaterial* base_material;
		Qt3DCore::QEntity* Entities;

		Qt3DRender::QCamera* camera;
		Qt3DExtras::Qt3DWindow* view;
		Qt3DExtras::QOrbitCameraController* camController;

	public slots:
		
		void add_Mesh(QString&, Point_3&);
		


	};




	class MainApp : public QMainWindow {

		Q_OBJECT


	public:

		MainApp();
		~MainApp();
		vector<OBJECT_PARSER::STL_Object> STL_meshes;

		
		QWidget* MainWidget;
		QWidget* CamWidget;
		QGridLayout* MainLayout;

		QGridLayout* MenuLayout;
		QGridLayout* MainWidgetLayout;

		QPushButton* Load_STL_button;
		QPushButton* Grid_init_button;
		QPushButton* Set_LBM_param_button;	
		

		_Cam_Object* Cam_Object;
		_Main_Mesh_Object* Main_Mesh_Object;
		_LBM_Backend_thread* LBM_backend;
		
		QThread* LBM_thread;
	

		void start();

	signals:
		void Load_STL_Table_show();		
		void LBM_Backend_init_signal();
		void LBM_Backend_grid_setup_signal(vector<OBJECT_PARSER::STL_Object>);

		

	private:	

		void retranslate();
		void Cam_init();

		bool Main_Mesh_Widget_allocated = false;
		bool Cam_Widget_allocated = false;
		bool LBM_backend_allocated = false;



	private slots:


		void Load_Main_Mesh_Widget();	
		void Grid_init();
		void Set_LBM_param();
		void add_STL(OBJECT_PARSER::STL_Object&);
		

	};


}

