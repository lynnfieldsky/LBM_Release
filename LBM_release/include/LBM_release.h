#pragma once


#include <QtWidgets/QMainWindow>
#include "../UI/ui_MainWindow.h"
#include "datatype.h"

class LBM_release : public QMainWindow
{
    Q_OBJECT

public:
    LBM_release(QWidget *parent = Q_NULLPTR);

private:
    Ui::MainWindow ui;

private slots:
    void LBM_start();

};
