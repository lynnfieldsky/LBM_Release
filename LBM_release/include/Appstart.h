#pragma once

#include "datatype.h"
#include <QtWidgets/qdialog.h>
#include <QtWidgets/QApplication>
#include <QtWidgets/qframe.h>
#include <QtWidgets/QPushButton>
#include <QtWidgets/qgraphicseffect.h>
#include <qpainterpath.h>
#include <qlabel.h>
#include <QVBoxLayout>
#include <QPropertyAnimation>
#include <iostream>
#include "MainWindow.h"

extern _MAINAPP::MainApp* _MAIN_WIDGET;

namespace _APPSTART {

	class Appstart : public QWidget {

		Q_OBJECT


	public:
		Appstart();
		~Appstart();
		

	private:
		void retranslateSTART();
		void retranslateLBM();
		QPushButton* New_button;
		QPushButton* Load_button;
		QPushButton* Close_button;
		QPropertyAnimation* New_animation;
		QLabel *text_label;

		QPushButton* D2Q9_button;
		QPushButton* D3Q19_button;

		QPoint offset;

		void mousePressEvent(QMouseEvent*);
		void mouseMoveEvent(QMouseEvent*);


	private slots:
		void New();
		void Load();
		void D2Q9();
		void D3Q19();
		void show_LBM();

	};






}

