#include "include/MainWindow.h"

extern _MAINAPP::MainApp* _MAIN_WIDGET;

using namespace _MAINAPP;

_Main_Mesh_Object::~_Main_Mesh_Object() {

	emit this->finished();

}



_Main_Mesh_Object::_Main_Mesh_Object() {	

	parent_window = _MAIN_WIDGET;	

}

void _Main_Mesh_Object::TableInit() {
	
	Layouts.emplace_back(new QGridLayout());
	
	Table = new QTableWidget();
	Table->setObjectName("Table");
	Table->setGeometry(QRect(100, 100, 800, 400));
	Table->setColumnCount(1);
	Table->setColumnWidth(0, 600);
	//Table->setColumnWidth(1, 50);
	Table->setHorizontalHeaderLabels(QStringList("STL files"));
	Table->setRowCount(1);
	Table->setRowHeight(0, 30);
	Table_add_new = new QPushButton(Table);
	Table_add_new->setText(QCoreApplication::translate("Table", "Add STL files", nullptr));
	Layouts.back()->addWidget(Table_add_new, 0, 0);
	Layouts.back()->setSpacing(0);
	Layouts.back()->setContentsMargins(0, 0, 0, 0);
	Cell_widgets.emplace_back(new QWidget(Table));
	Cell_widgets.back()->setMaximumSize(QSize(Table->columnWidth(0), Table->rowHeight(0)));
	Cell_widgets.back()->setLayout(Layouts.back());

	Table->setCellWidget(0, 0, Cell_widgets.back());

	connect(Table_add_new, SIGNAL(released()), this, SLOT(read_STL()));

	connect(this, SIGNAL(add_STL(QString&, Point_3&)), parent_window->Cam_Object, SLOT(add_Mesh(QString&, Point_3&)));
	connect(this, SIGNAL(add_STL(OBJECT_PARSER::STL_Object&)), parent_window, SLOT(add_STL(OBJECT_PARSER::STL_Object&)));

	Table->show();

	
}



si32 _Main_Mesh_Object::read_STL() {

	QStringList file_list = FileDialog(nullptr, "Load STL File");

	foreach(QString path, file_list) {

		OBJECT_PARSER::STL_Object loaded_STL = OBJECT_PARSER::Load_STL(path, 0.001); //처음 읽었을 때
		Point_3 center((loaded_STL.max_coords - loaded_STL.min_coords) * 0.5);
		Table->setRowCount(Table->rowCount() + 1);

		Table_buttons.emplace_back(new QPushButton(path));
		Table_buttons.back()->setStyleSheet("text-align: left; border: none;");
		Layouts.back()->addWidget(Table_buttons.back(), 0, 0);
		Layouts.back()->setContentsMargins(0, 0, 0, 0);
		Cell_widgets.back()->setLayout(Layouts.back());
		Table->setCellWidget(Table->rowCount() - 2, 0, Cell_widgets.back());
		Cell_widgets.emplace_back(new QWidget());
		Layouts.emplace_back(new QGridLayout(Table));
		Layouts.back()->setContentsMargins(0, 0, 0, 0);
		Layouts.back()->setSpacing(0);
		Layouts.back()->addWidget(Table_add_new, 0, 0);

		Cell_widgets.back()->setLayout(Layouts.back());

		Table->setCellWidget(Table->rowCount() - 1, 0, Cell_widgets.back());

		emit add_STL(path, center);
		emit add_STL(loaded_STL);

	}

	return 0;


}

void _Main_Mesh_Object::Table_show() {
	
	if (initialized == false) {
		
		TableInit();
		initialized = true;
	}

	Table->show();
	Table->activateWindow();
	Table->raise();
}




