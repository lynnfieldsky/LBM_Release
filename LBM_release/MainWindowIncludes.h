#pragma once
#include <utility>

#include "datatype.h"
#include "LBM_D3Q19_dat.h"
#include "Object_parser.h"

#include <qmainwindow.h>
#include <QtWidgets/QPushButton>
#include <QMessageBox>


#include <QtWidgets/QApplication>
#include <qfiledialog.h>
#include <Qstring>
#include <string>
#include <QThread>
#include <QThreadPool>
#include <QTableWidget>
#include <QHBoxLayout>
#include <QVector>


#include <Qt3DCore/qentity.h>
#include <Qt3DExtras/qt3dwindow.h>
#include <Qt3DExtras/qorbitcameracontroller.h>

#include <Qt3DRender/qcamera.h>
#include <Qt3DRender>
#include <Qt3DExtras>
