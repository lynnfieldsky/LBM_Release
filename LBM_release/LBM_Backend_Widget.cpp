#include "include/MainWindow.h"

extern _MAINAPP::MainApp* _MAIN_WIDGET;


_MAINAPP::_LBM_Backend_thread::_LBM_Backend_thread() {

	parent_window = _MAIN_WIDGET;

}

_MAINAPP::_LBM_Backend_thread::~_LBM_Backend_thread() {

	std::cout << "deleted" << std::endl;


}

LBM_D3Q19::_INPUT _MAINAPP::_LBM_Backend_thread::get_INPUT() const { return PROJECT_PARAMS.get_INPUT(); }
bool _MAINAPP::_LBM_Backend_thread::is_initialized() const { return PROJECT_PARAMS.INPUT.initialized; }


si32 _MAINAPP::_LBM_Backend_thread::initialize() {

	PROJECT_PARAMS.set_INPUT(LBM_D3Q19::_INPUT(true, true, LBM_D3Q19::Processors::MULTI_CORE, false, 1, 500, 10, 0.001, FLUIDS(MATERIAL_AIR), vector<ui32>(0)));
	
	//TODO ����� �Է�ĭ

	PROJECT_PARAMS.INPUT.OPENMP_THREAD_COUNT = si32(ceil(std::thread::hardware_concurrency() * 0.8));
	omp_set_num_threads(PROJECT_PARAMS.INPUT.OPENMP_THREAD_COUNT);

}

si32 _MAINAPP::_LBM_Backend_thread::grid_setup(vector<OBJECT_PARSER::STL_Object> Mesh) {

	switch (PROJECT_PARAMS.INPUT.target_Processor) {

	case LBM_D3Q19::Processors::SINGLE_CORE:
		grid_gen_st(Mesh);
		break;
	case LBM_D3Q19::Processors::MULTI_CORE:
		grid_gen_omp(Mesh);
		break;
	case LBM_D3Q19::Processors::GPU:
		grid_gen_gpu(Mesh);
		break;
	default:
		grid_gen_st(Mesh);
		break;
	}

	return 0;


}





si32 _MAINAPP::_LBM_Backend_thread::grid_gen_omp(vector<OBJECT_PARSER::STL_Object> Mesh){
	std::cout << this->thread() << " LBM" << std::endl;
	//0.0001 grid to desired dx grid
	for (si32 object = 0; object < Mesh.size(); object++) {

		#pragma omp parallel for
		for (si32 tri = 0; tri < Mesh[object].Tris.size(); tri++) {

			Mesh[object].Tris[tri] *= Mesh[object].unit;
			Mesh[object].Tris[tri] /= PROJECT_PARAMS.INPUT.initial_dx;

		}

		Mesh[object].min_coords *= Mesh[object].unit;
		Mesh[object].min_coords /= PROJECT_PARAMS.INPUT.initial_dx;
		Mesh[object].max_coords *= Mesh[object].unit;
		Mesh[object].max_coords /= PROJECT_PARAMS.INPUT.initial_dx;

		Mesh[object].tree_init_size = Mesh[object].max_coords - Mesh[object].min_coords;


		for (ui32 power = 0; power < INT_MAX; power++) {

			ui32 curr_pow = std::pow(2, power);
			if (curr_pow >= Mesh[object].tree_init_size[0] && curr_pow >= Mesh[object].tree_init_size[1] && curr_pow >= Mesh[object].tree_init_size[2]) {

				Mesh[object].tree_division_level = power;
				break;
			}			

		}

		




	




		

	}


	return 0;

}

si32 _MAINAPP::_LBM_Backend_thread::grid_gen_st(vector<OBJECT_PARSER::STL_Object> Mesh) {
	return 0;
}

si32 _MAINAPP::_LBM_Backend_thread::grid_gen_gpu(vector<OBJECT_PARSER::STL_Object> Mesh) {
	return 0;
}