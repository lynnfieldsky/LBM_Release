#include "qapplication.h"
#include "include/GLOBAL_UI_H.h"

_MAINAPP::MainApp* _MAIN_WIDGET;
_APPSTART::Appstart* _START_WIDGET;

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);
    //QApplication a(argc, argv);
    _MAIN_WIDGET = new _MAINAPP::MainApp();
    _MAIN_WIDGET->setParent(nullptr);
    _START_WIDGET = new _APPSTART::Appstart();
    _START_WIDGET->setParent(nullptr);

    //_MAIN_WIDGET = &_MAIN_WIDGET_ADD;

    //_START_WIDGET = &_START_WIDGET_ADD;

    _START_WIDGET->show(); 

    return app.exec();
}
