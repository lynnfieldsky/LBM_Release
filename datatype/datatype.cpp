﻿
#include "datatype.h"
#include "framework.h"
#include <cmath>

using namespace LBM_DATATYPE;

//TODO retranslate

Point_3::Point_3() { _x = 0; _y = 0; _z = 0; }
Point_3::Point_3(const fl64& value) { _x = value; _y = value; _z = value; }
Point_3::Point_3(const fl64& a, const fl64& b, const fl64& c) { _x = a; _y = b; _z = c; }
Point_3::Point_3(const Point_3& P) { _x = P.x(); _y = P.y(); _z = P.z(); }

inline fl64 Point_3::x() const { return _x; }
inline fl64 Point_3::y() const { return _y; }
inline fl64 Point_3::z() const { return _z; }
inline Point_3 Point_3::value() const { return Point_3(_x, _y, _z); }

const fl64& Point_3::operator[](const si32& index) const { return *(&_x + index); }
const fl64& Point_3::operator[](const ui32& index) const { return *(&_x + index); }

fl64& Point_3::operator[](const si32& index) { return *(&_x + index); }
fl64& Point_3::operator[](const ui32& index) { return *(&_x + index); }



bool Point_3::operator==(const Point_3& rhs) const { return (this->dist(rhs)) < 1e-7; }
bool Point_3::operator!=(const Point_3& rhs) const { return x() != rhs.x() || y() != rhs.y() || z() != rhs.z(); }
bool Point_3::operator<(const Point_3& rhs) const {

	if (x() > rhs.x()) {

		return false;

	}

	else if (y() > rhs.y()) {

		return false;

	}

	else if (z() > rhs.z()) {

		return false;

	}

	else if (*this == rhs) {

		return false;

	}

	else {

		return true;

	}

}

Point_3 Point_3::operator+() const { return Point_3(x(), y(), z()); }
Point_3 Point_3::operator-() const { return Point_3(-x(), -y(), -z()); }

Point_3 Point_3::operator+(const Point_3& rhs) const { return Point_3(x() + rhs.x(), y() + rhs.y(), z() + rhs.z()); }
Point_3 Point_3::operator-(const Point_3& rhs) const { return Point_3(x() - rhs.x(), y() - rhs.y(), z() - rhs.z()); }
Point_3 Point_3::operator*(const Point_3& rhs) const { return Point_3(x() * rhs.x(), y() * rhs.y(), z() * rhs.z()); }
Point_3 Point_3::operator/(const Point_3& rhs) const { return Point_3(x() / rhs.x(), y() / rhs.y(), z() / rhs.z()); }

Point_3 Point_3::operator+(const fl64& rhs) const { return Point_3(x() + rhs, y() + rhs, z() + rhs); }
Point_3 Point_3::operator-(const fl64& rhs) const { return Point_3(x() - rhs, y() - rhs, z() - rhs); }
Point_3 Point_3::operator*(const fl64& rhs) const { return Point_3(x() * rhs, y() * rhs, z() * rhs); }
Point_3 Point_3::operator/(const fl64& rhs) const { return Point_3(x() / rhs, y() / rhs, z() / rhs); }

Point_3& Point_3::operator+=(const Point_3& rhs) { _x += rhs.x(); _y += rhs.y(); _z += rhs.z(); return *this; }
Point_3& Point_3::operator-=(const Point_3& rhs) { _x -= rhs.x(); _y -= rhs.y(); _z -= rhs.z(); return *this; }
Point_3& Point_3::operator*=(const Point_3& rhs) { _x *= rhs.x(); _y *= rhs.y(); _z *= rhs.z(); return *this; }
Point_3& Point_3::operator/=(const Point_3& rhs) { _x /= rhs.x(); _y /= rhs.y(); _z /= rhs.z(); return *this; }


Point_3& Point_3::operator+=(const fl64& rhs) { _x += rhs; _y += rhs; _z += rhs; return *this; }
Point_3& Point_3::operator-=(const fl64& rhs) { _x -= rhs; _y -= rhs; _z -= rhs; return *this; }
Point_3& Point_3::operator*=(const fl64& rhs) { _x *= rhs; _y *= rhs; _z *= rhs; return *this; }
Point_3& Point_3::operator/=(const fl64& rhs) { _x /= rhs; _y /= rhs; _z /= rhs; return *this; }

fl64 Point_3::dot_product(const Point_3& rhs) const { return x() * rhs.x() + y() * rhs.y() + z() * rhs.z(); }
Point_3 Point_3::cross_product(const Point_3& rhs) const { return Point_3(y() * rhs.z() - x() * rhs.y(), z() * rhs.x() - x() * rhs.z(), x() * rhs.y() - y() * rhs.x()); }

Point_3& Point_3::square() { _x = x() * x(); _y = y() * y(); _z = z() * z(); return *this; }
Point_3& Point_3::sqrt() { _x = std::sqrt(x()); _y = std::sqrt(y()); _z = std::sqrt(z()); return *this; }
fl64 Point_3::sum() const { return x() + y() + z(); }
fl64 Point_3::length() const { return std::sqrt((Point_3(_x, _y, _z).square()).sum()); }
Point_3& Point_3::norm() { return *this /= this->length(); }


fl64 Point_3::dist(const Point_3& rhs) const { return std::sqrt(((*this - rhs).square()).sum()); }


struct Point_3::X_comparator {

	bool operator ()(const Point_3& lhs, const Point_3& rhs) const { return lhs.x() < rhs.x(); }

};
struct Point_3::Y_comparator {

	bool operator ()(const Point_3& lhs, const Point_3& rhs) const { return lhs.y() < rhs.y(); }

};
struct Point_3::Z_comparator {

	bool operator ()(const Point_3& lhs, const Point_3& rhs) const { return lhs.z() < rhs.z(); }

};



fl64 Mesh_tri::det(Point_3 tri_points[3], ui32 n = 3) {

	fl64 det = 0;	
	Point_3 submatrix[3];
	if (n == 2)
		return ((tri_points[0].x() * tri_points[1].y()) - (tri_points[1].x() * tri_points[0].y()));
	else {
		for (ui32 x = 0; x < n; x++) {
			ui32 subi = 0;
			for (ui32 i = 1; i < n; i++) {
				ui32 subj = 0;
				for (ui32 j = 0; j < n; j++) {
					if (j == x)
						continue;
					submatrix[subi][subj] = tri_points[i][j];
					subj++;
				}
				subi++;
			}
			det = det + (std::pow(-1, x) * tri_points[0][x] * Mesh_tri::det(submatrix, n - 1));
		}
	}
	return det;
}

Mesh_tri::Mesh_tri() {


}

Mesh_tri& Mesh_tri::operator*=(const fl64& rhs) { tri_points[0] *= rhs; tri_points[1] *= rhs; tri_points[2] *= rhs; normal *= rhs; return *this; }
Mesh_tri& Mesh_tri::operator/=(const fl64& rhs) { tri_points[0] /= rhs; tri_points[1] /= rhs; tri_points[2] /= rhs; normal /= rhs; return *this; }


Point_3 Mesh_tri::anorm2() {

	Point_3 norm;

	for (si32 i = 0; i < 3; i++) {
		norm[i] = tri_points[0].length();
	}
	return norm;

}

FLUIDS::FLUIDS() {


}

//TODO bool error
FLUIDS::FLUIDS(ui08 mat) { 
		
	err = set_material(mat);
	
	switch (mat) {

	case MATERIALS::MATERIAL_AIR:
		set_speed_of_sound(340.);
		set_density(1.225);
		set_dynamic_viscosity(1.45e-5);
		break;
	case MATERIALS::MATERIAL_WATER:
		set_speed_of_sound(1500.);
		set_density(1000);
		set_dynamic_viscosity(1e-6);
		break;
	case MATERIALS::MATERIAL_BLOOD:
		set_speed_of_sound(1500.);
		set_density(1000.);
		set_dynamic_viscosity(3e-6);
		break;

	}

}

bool FLUIDS::set_material(ui08 mat) {
	
	Material = mat;
	return true;

}

void FLUIDS::set_speed_of_sound(fl64 cs) {

	speed_of_sound = cs;

}

void FLUIDS::set_density(fl64 rho) {

	density = rho;

}

void FLUIDS::set_dynamic_viscosity(fl64 nu) {

	dyn_viscosity = nu;

}

fl64 FLUIDS::get_speed_of_sound() const {

	return speed_of_sound;

}

fl64 FLUIDS::get_density() const {

	return density;

}

fl64 FLUIDS::get_dynamic_viscosity() const {

	return dyn_viscosity;

}

std::string FLUIDS::get_material() const {

	std::string val;

	if (Material == NONE) {

		val = "Material is not defined yet";

	}

	else if (Material == MATERIAL_AIR) {

		val = "Air";

	}

	else if (Material == MATERIAL_WATER) {

		val = "Water";

	}

	else if (Material == MATERIAL_BLOOD) {

		val = "Blood";

	}

	else if (Material == MATERIAL_USER_INPUT) {

		val = "User Defined Fluid";

	}

	else {

		val = "Undefined";

	}

	return val;
}






