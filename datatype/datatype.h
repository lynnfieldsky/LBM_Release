
#ifndef LBM_DATATYPE_DEF
#define LBM_DATATYPE_DEF

#include <string>
#include "framework.h"



#define CACHE_SIZE (sizeof(LBM_DATATYPE::fl64) * 8)

namespace LBM_DATATYPE {
	

	typedef unsigned char ui08;
	typedef signed char si08;
	typedef unsigned int ui32;
	typedef signed int si32;

	typedef unsigned __int64 ui64;
	typedef signed __int64 si64;

	typedef float fl32;
	typedef double fl64;

	//static const size_t cache_size = std::hardware_destructive_interference_size;
	

	const enum __declspec(align(CACHE_SIZE)) MATERIALS {

		NONE = 0b00000000,
		MATERIAL_AIR = 0b0000010,
		MATERIAL_WATER = 0b0000100,
		MATERIAL_BLOOD = 0b0001000,
		MATERIAL_USER_INPUT = 0b10000000,
		MATERIAL_LBM = 0b11111111

	};
	
	
	class Point_3 {

	private:
		
		fl64 _x;
		fl64 _y;
		fl64 _z;

	public:

		Point_3();
		Point_3(const fl64& value);
		Point_3(const fl64& a, const fl64& b, const fl64& c);
		Point_3(const Point_3& P);


		inline fl64 x() const;
		inline fl64 y() const;
		inline fl64 z() const;
		inline Point_3 value() const;

		const fl64& operator[](const si32& index) const;	
		const fl64& operator[](const ui32& index) const;

		fl64& operator[](const si32& index);
		fl64& operator[](const ui32& index);

		bool operator==(const Point_3& rhs) const;
		bool operator!=(const Point_3& rhs) const;
		bool operator<(const Point_3& rhs)const;

		Point_3 operator+()const;
		Point_3 operator-()const;
		
		Point_3 operator+(const Point_3& rhs) const;		
		Point_3 operator-(const Point_3& rhs) const;		
		Point_3 operator*(const Point_3& rhs) const;		
		Point_3 operator/(const Point_3& rhs) const;
		
		Point_3 operator+(const fl64& rhs) const;		
		Point_3 operator-(const fl64& rhs) const;		
		Point_3 operator*(const fl64& rhs) const;		
		Point_3 operator/(const fl64& rhs) const;

		Point_3& operator+=(const Point_3& rhs);		
		Point_3& operator-=(const Point_3& rhs);		
		Point_3& operator*=(const Point_3& rhs);		
		Point_3& operator/=(const Point_3& rhs);
		
		Point_3& operator+=(const fl64& rhs);		
		Point_3& operator-=(const fl64& rhs);		
		Point_3& operator*=(const fl64& rhs);		
		Point_3& operator/=(const fl64& rhs);
		
		fl64 dot_product(const Point_3& rhs) const;
		
		Point_3 cross_product(const Point_3& rhs) const;

		Point_3& square();
		Point_3& sqrt();
		fl64 sum() const;
		fl64 length() const;
		Point_3& norm();
		
		fl64 dist(const Point_3& rhs) const;

		struct X_comparator;
		struct Y_comparator;
		struct Z_comparator;

	
	};

	class Point_2;

	 class Mesh_tri {

	public:
		Mesh_tri();
		Point_3 tri_points[3];
		Point_3 normal;
		ui32 idx = 0;

		fl64 det(Point_3[3], ui32);
		Point_3 anorm2();
		Mesh_tri& operator*=(const fl64& rhs);
		Mesh_tri& operator/=(const fl64& rhs);			 

	};

	 class TREE_CELL3D {

	 private:
		 fl64 i = 0;
		
	 };





	class FLUIDS {

	public:
		
		FLUIDS();
		FLUIDS(ui08);
		std::string get_material() const;
		fl64 get_speed_of_sound() const;
		fl64 get_density() const;
		fl64 get_dynamic_viscosity() const;

		bool set_material(ui08);
		void set_speed_of_sound(fl64);
		void set_density(fl64);
		void set_dynamic_viscosity(fl64);



	private:

		ui08 Material = 0;
		bool err = 0;
		fl64 speed_of_sound;
		fl64 density;
		fl64 dyn_viscosity;


	};
	
	

}

#endif