#pragma once

#include <array>
#include <vector>
#include <queue>
#include <tuple>
#include <string>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <cmath>
#ifdef _WIN32
#define DELIMITERSTR "\\"
#include <direct.h>
#else
#define DELIMITERSTR "/"
#include <sys/stat.h>
#define _mkdir(fn) mkdir(fn, 0755)
#endif

using namespace std;

namespace LBM_DATATYPE {

	typedef unsigned char ui08;
	typedef signed char si08;
	typedef unsigned int ui32;
	typedef signed int si32;

	typedef unsigned __int64 ui64;
	typedef signed __int64 si64;

	typedef float fl32;
	typedef double fl64;


}